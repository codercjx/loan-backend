'use strict';

import logger from './app/util/logger'
import config from './config/config'
import express from './config/myexpress'

let app = express();
app.listenAsync(config.port).then(function(){
    //console.log --- 控制台输出 
    logger.info(`Server started on port ${config.port}.`)
});
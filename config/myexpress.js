'use strict';

import path from "path"   //node中工具--路径
import util from 'util'   //node中
import express from "express"
import bodyParser from 'body-parser'
import session from "express-session";

export default () => {
    var app = express();
    //跨域  cors
    app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');//自定义中间件，设置跨域需要的响应头。
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');  //允许任何方法
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,X-Session-Token');   //允许任何类型
        next();
    })


    //session支持
    app.use(session({
        secret: "secret",//签名
        resave: true,    //是否允许session重新设置
        saveUninitialized: false,//是否保存未初始化
        cookie: {
            maxAge: 1000 * 60 * 30, //session有效时间,单位是ms
        }
    }));


    //对前端传入参数解析
    app.use(bodyParser.json()) //创建application/json 解析
    app.use(bodyParser.urlencoded({extended: true}))// 创建 application/x-www-form-urlencoded 解析
    // http://localhost:3333/info
    // app.get("/info", function(req, res){
        
    // })

    //路由
    //require()方法加载的文件要是绝对路劲。 process.cwd() === 得到当前程序执行的目录
    require(process.cwd() + "/app/routes/user.route.js")(app)

    // app.listen("3333",function () {
    //     console.log("sever running.....");
    // });
    //将异步方法(嵌套回调)变为Promise  app.listen是异步
    //将异步回调包装成promise 增加一个方法listenAsync
    app.listenAsync = util.promisify(app.listen);

    

    return app;
}
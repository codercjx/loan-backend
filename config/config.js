'use strict'

const config = {
    // timezone: "+08:00", //时区  北京时间 东8区
    port: process.env.PORT || 3333, //第一个没有值就用第二个
    db: { //数据库配置对象
        database: "jindu_loan",//数据库名
        username: "root",//账号
        password: "root",//密码
        host: "127.0.0.1",//ip地址
        port: 3306,   //数据库端口
        timezone: "+8:00",//时区
        dialect: "mysql", //方言
        define: {    //模型默认定义
            timestamps: false, //取消默认添加属性
        }
    }
}

export default config
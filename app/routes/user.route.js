'use strict'

import express from 'express'  //依赖包
import userCtrl from '../controllers/user.ctrl'
import LoanStatus from '../util/status.loan'
const router = express.Router();  //路由 地址  express内置

export default function(app){ //从myexpress

    //登录路由地址  /api/user/login----userCtrl.login方法
    router.route('/user/login').post(userCtrl.login)

    //获取用户角色信息
    router.route("/user/info").get(userCtrl.info);

    //获取所有用户和角色信息
    router.route("/user/list").get(userCtrl.list);

    //获取用户===测试分页
    router.route('/user/list2').get(userCtrl.list2);

    //创建用户
    router.route('/user/create').post(userCtrl.createUser);

    //更新用户信息
    router.route('/user/update').post(userCtrl.updateUser);

    //删除用户
    router.route('/user/delete/:id').post(userCtrl.deleteUser);



    //拦截 每一次请求都经过它过滤 OPTIONS--ajax跨域（协议 域名 端口）
    let checkLogin = (req, res, next)=>{
        console.log("请求拦截日志。。。。。")
        //req.originalUrl == 请求源
        if (req.originalUrl === '/api/user/login') {
            next()
        } else if (req.session.userInfo 
            && req.session.userInfo.token == req.headers['token']) {
                console.log("正常进入")
                next()
        } else {
            //LoanStatus.RESULT 统一数据格式
            // LoanStatus.RESULT.data = {code: 10000, msg: "请登录。。。"}
            let result = {
                code: 10000,
                data: {
                    msg: "请登录。。。"
                }
            };
            //result
            res.send(result)
            // res.send(LoanStatus.RESULT.data)
        }
    }
    app.use(checkLogin)


    // 给所有路由加前缀
    app.use("/api", router);
}

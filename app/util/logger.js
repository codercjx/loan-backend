'use strict'
//日志包 输出级别：info debug error
const winston = require("winston");//node包
const logger = winston.createLogger({
    level: 'info', //级别
    //输出的位置
    transports:[
        new winston.transports.Console(),//输出在控制台
        new winston.transports.File({filename:'./logs/applog.log'})
    ]
})

export default logger;

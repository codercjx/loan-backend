'use strict'

import * as userService from "../services/user.service"
import logger from '../util/logger'
import LoanStatus from '../util/status.loan'

const operations = {
    //登录接口  业务逻辑 记录登录的用户信息 session 
    //返回统一的数据格式，便于前端处理
    login: function(req, res) {
        let {account, password} = req.body;
        logger.info(req.body);
        logger.info("======> login..."+account+","+password)
        userService.findUser(account,password)
        .then(data => {
            // console.log(data);
            if (data){
                LoanStatus.RESULT.data = {
                    id: data.id,
                    account: account,
                    token: new Date().getTime()
                };
                //===>>保存用户信息到session
                req.session.userInfo = LoanStatus.RESULT.data;
                res.status(200).json(LoanStatus.RESULT);
                // console.log(req.session);

            } else {
                res.status(404).send("读取数据失败！")
            }
        })
        //返回200(http code 200) + json数据   json、send()都是一样的
        // res.status(200).json(data);
    },
    //获取用户角色信息，根据用户id查找相关信息
    info : function (req, res) {
        console.log(req.session);
        //用户id(登录的时候保存在 session中)
        let uid = req.session.userInfo.id;
        userService.findRoleByUid(uid)
        .then(r => {
            LoanStatus.RESULT.data = {
                role: [{id: r.id,name: r.role_name}]
            };
            // console.log(LoanStatus.RESULT);
            res.status(200).json(LoanStatus.RESULT);
        })
        
    },
    //获取所有用户和角色信息,只能登录后才能访问？
    list: function (req,res) {
        //判断是否登录(可以判断是否有有用户信息 且token与传入的相同)
        // if (req.session.userInfo && req.session.userInfo.token == req.headers["token"]) {
            userService.findAll().then(d => {
                LoanStatus.RESULT.data = d;
                res.status(200).json(LoanStatus.RESULT);
            });
        // } else {
        //     //前端没有登录或者没有携带cookie或者没有传token
        //     res.status(200).json({
        //         msg: "请求错误"
        //     });
        // }
    },
    //获取用户数据==测试分页  pageNo 从1开始 pageSize(每页显示多数条数据)  name(模糊查询)
    list2: function (req,res) {
        let {pageNo, pageSize, name} = req.query;
        if (pageNo && pageSize) {
            // pageNo = pageNo - 1;
            userService.findAll2(pageNo, pageSize, name)
            .then(d => {
                console.log(d)
                LoanStatus.RESULT.data = d;
                res.status(200).json(LoanStatus.RESULT)
            })
            .catch(msg=>{
                console.log(msg)
            })
        } else {
            //传递数据不合法

        }   
    },
    //创建用户   前端传入 account、password、real_name这三个参数
    createUser: function (req,res) {
        const user = req.body;
        logger.info("create user :"+JSON.stringify(user))
        if (user) {
            userService.createUser(user)
                .then(data => {
                    if (data.code == 1) {
                        res.status(200).json({
                            code: 20001,
                            data: {msg: "账号已经存在，请求更换账号"}
                        });
                    } else {
                        LoanStatus.RESULT.data = {msg: "创建成功"};
                        res.status(200).json(LoanStatus.RESULT);
                    }
                    
               })
               .catch(msg => {
                   console.log(msg);
               })
        } else {
            res.status(400).json({"msg":"user表单为空"});
        }
    },
    //修改用户 user==表单 唯一标识id,根据id找到数据库中的那个用户，再修改传过来的信息
    updateUser: function(req, res) {
        const user = req.body;
        logger.info("update user :"+JSON.stringify(user));
        userService.updateUser(user)
            .then(data => {
                LoanStatus.RESULT.data = {status: true};
                res.status(200).json(LoanStatus.RESULT);
            })
            .catch(msg => {
                console.log(msg)
            })
    },
    //删除用户  id
    deleteUser: function(req, res) {
        const id = req.params.id;//post请求,参数(id)再url上
        logger.info("delete user :"+id)
        userService.deleteUser(id)
            .then(data => {
                if (data.code == 1) {
                    //删除成功
                    LoanStatus.RESULT.data = {msg: "删除成功"}
                    res.status(200).json(LoanStatus.RESULT)
                } else if (data.code == 2) {
                    //数据库中没有这个用户，删除失败
                    res.status(200).json({
                        code: 20002,
                        data: {msg: "该用户不存在"}
                    });
                }
                
            })
    }
}

export default operations

"use strict"

// 服务层 操作数据库
import models from "../models"
const User = models.jd_user; //用户模型
const Role = models.jd_role; //角色模型
const UserRole = models.jd_user_role; //用户角色关系模型


/**
 * 查找用户
 * @param {*} account 账号
 * @param {*} password 密码
 */
export function findUser (account,password) {
    //findOne方法返回查找的那个用户对象,也可以使用promise写法 User.findOne().then(data => {}),形参data就是查找的那个用户对象
    return User.findOne({ //查询一条数据 findOne是 sequelize框架 自带的方法
        where: { //查询条件 sql
            account: account,
            password: password
        }
    });
}

/**
 * 根据用户的id查找角色 async await
 * @param {*} uid 
 */
export async function findRoleByUid (uid) {
    //获取用户角色关系对象,
    //比如：用户1的role_id(角色id)是1，用户2的role_id是3。
    //再通过角色id得到该用户具体的角色信息
    var userRole = await UserRole.findOne({
        where: {
            user_id: uid
        }
    });
    //通过用户角色关系对象，得到角色对象。获取到了用户具体角色
    var role = await Role.findOne({
        where: {
            id: userRole.role_id
        }
    });
    return role;
}

/**
 * 获取所有用户和角色信息  sql:select insert update delete
 */
export async function findAll () {
    //将读取的数据存入User，便于使用面向对象
    var d = await models.sequelize
            .query(`SELECT u.account, u.password, u.reg_time, u.creator, r.role_name 
            from jd_user u left join jd_user_role ur on u.id=ur.user_id 
            left join jd_role r on r.id=ur.role_id order by u.id desc `,{model:User})
    return d;
}

/**
 * 
 * @param {*} pageNo 第几页(从1开始)
 * @param {*} pageSize 每页显示多少条数据
 * @param {*} name 
 */
export async function findAll2(pageNo, pageSize, name) {
    var limit = pageSize;
    var offset = (pageNo - 1) * pageSize; //pageNo 0
    let result = {}
    if (name) {
        //===>>>模糊查询数据  一页的记录数  10
        var d = await models.sequelize
                .query(`select * from jd_user where real_name like ? limit ${offset}, ${limit}`,
                {replacements:['%'+name+'%'], model: User})
        result.data = d;
        //模糊查询总记录
        var count = await models.sequelize
            .query("select count(*) num from jd_user where real_name like ?",{replacements:['%'+name+'%']});
        if (count) {// count [[num:30]]
            //总条数
            result.rows = count[0][0].num;
            //总页数
            result.pages = Math.ceil(count[0][0].num/pageSize)
        }
    } else {
        //拿到一页数据 sequelize findAll findOne
        var d = await User.findAll({
            limit: Number(limit),  //一页显示条数
            offset: Number(offset), //从哪一条数据开始读  行号
            order: [
                ["id", "DESC"]
            ]
        })
        result.data = d;
        //总页数 总条数 es7
        //用sql语句查询js_user表的所有数据
        var count = await models.sequelize.query("select count(*) num from jd_user")
        if (count) {
            console.log(count)
            //总条数
            result.rows = count[0][0].num;
            //总页数
            result.pages = Math.ceil(count[0][0].num/pageSize)
        }
    }
    return result;
}

/**
 * 用户添加 user--表单 orm
*/
export async function createUser (user) {
    //User--模型对象  create--sequelize
    //===>>先判断账号是否存在
    let result_user = await User.findOne({ //查询一条数据 findOne是 sequelize框架 自带的方法
        where: { //查询条件 sql
            account: user.account,
        }
    });
    // console.log(result_user);
    if (result_user) {
        //数据库存在这个账号,不能创建
        return {code: 1};
    } else {
        await User.create(user);
        return {code: 2};
    }
    // return User.create(user);
}

/**
 * 修改用户 id--拿到数据--更新 sequelize
*/
export function updateUser(userForm) {
    if (userForm && userForm.id) {
        return User.findByPk(userForm.id) //findById(之前的方法)==findByPk
                   .then(u => { //u==User.findByPk
                       return u.update(userForm); //更新
                   })
    }
}

/**
 * 删除用户  id
*/
export async function deleteUser(id) {
    //===>>先判断账号是否存在
    let result_user = await User.findOne({ //查询一条数据 findOne是 sequelize框架 自带的方法
        where: { //查询条件 sql
            id: id,
        }
    });
    if (result_user) {
        //有这个用户，可以删除
        await User.destroy({
            where: {
                id: id
            }
        });
        return {code: 1};
    } else {
        //没有这个用户,不能删除
        return {code: 2};
    }
    
}
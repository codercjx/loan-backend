## 一、package.json安装包详解:

- body-parser: http请求body解析
- cookie-parser: 解析cookie
- express: 基于Node.js 平台,快速、开放、极简的 Web 开发框架
- express-session: 服务端的session
- mysql2: 使用sequelize必须要装mysql2
- nodemon: 监听项目代码变化，代码保存后自动启动服务(需要在package.json中scripts属性中添加字段通过nodemon运行项目)
- sequelize: sequelize是一个基于promise的nodejs ORM
- winston: 是Node.js最流行的日志框架之一,设计为一个简单通用的日志库

- babel-cli: babel转码 es6->es5
- babel-preset-env: 自动根据环境转码
- babel-plugin-add-module-exports: 使node支持export用法
- myexpress---自定义组件
- server---引用myexpress组件并启动服务

## 二、记录

### 我的记录：

- 1、要在电脑上运行项目先要安装  yarn :npm i yarn -g
- 2、然后在全局安装 babel-node(用于启动项目的): npm i babel-node -g
  ====>>>babel-node server.js是通过 .babelrc文件中的配置去转换的。所以使用了babel-node 就要在项目根目录中添加.babelrc文件。
      为什么要使用babel-node server.js 而不直接用node server.js？？？？？
      因为server.js、项目中其他文件中用到了es6语法，直接node server.js有的es6语法不识别，所以要babel-node把不识别的es6语法转换成es5。



## 测试项目接口：
     ### 先运行：npm run start
    - （其实就是在执行在package.json文件中 scripts: {"start": "babel-node server.js"}）
器地址栏: http://127.0.0.1:3333/api/user/login

    安装了nodemon之后也可以运行: npm run dev来开启项目(代码保存后可以自动重启服务)
    安装nodemon之后，package.json中scripts属性添加字段: "dev": "nodemon server.js --exec babel-node"


